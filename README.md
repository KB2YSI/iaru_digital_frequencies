# IARU_digital_frequencies

## Purpose

This is frequency data collected from all three IARU regions band plans where digital communications are allowed. Since each member nation can have different rules and regulations, this will not be completely accurate for each country within a region.

## Sources

* [IARU Band plans](https://www.iaru.org/on-the-air/band-plans)
  * [Region 1](https://www.iaru-r1.org/on-the-air/band-plans/)
    * [HF](https://www.iaru-r1.org/wp-content/uploads/2021/06/hf_r1_bandplan.pdf), [VHF](http://www.iaru-r1.org/wp-content/uploads/2020/12/VHF-Bandplan.pdf), [SHF](https://www.iaru-r1.org/wp-content/uploads/2020/12/SHF-Bandplan.pdf), [μWave](http://www.iaru-r1.org/wp-content/uploads/2020/12/%C2%B5W-Bandplan.pdf)
  * [Region 2](https://www.iaru-r2.org/en/reference/band-plans/)
  * [Region 3](https://www.iaru.org/wp-content/uploads/2020/01/R3-004-IARU-Region-3-Bandplan-rev.2.pdf)

## Something

A very useful tool to [convert frequencies](https://www.rapidtables.com/convert/frequency/index.html)

## Usage

Use this data in any manner you wish.

## Support

Please use the [issue tracker for any issues](https://gitlab.com/KB2YSI/iaru_digital_frequencies/-/issues).

## Road map

I would like to automate this, but parsing the different PDFs might be too hard and not worth the amount of work for how infrequent this data is likely to change.

## Contributing

If you would like to contribute to this effort please contact me.

## Authors and acknowledgment

Original author: Don Rhodes KB2YSI

## License

Not sure if this is really needed

## Project status

2023-0515 - Initial repository creation
